<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210219224126 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Remove unused url column';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('DROP INDEX idx_url ON review');
        $this->addSql('ALTER TABLE review DROP url');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE review ADD url VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('CREATE INDEX idx_url ON review (url)');
    }
}
