<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220222102904 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Move online flag from member to peer';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE peer ADD online TINYINT(1) NOT NULL');
        $this->addSql('CREATE INDEX idx_online ON peer (online)');
        $this->addSql('DROP INDEX idx_online ON member');
        $this->addSql('DROP INDEX idx_peers ON review');
        $this->addSql('CREATE INDEX idx_peers ON review (peers)');
        $this->addSql('UPDATE peer SET online = 1');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('CREATE INDEX idx_online ON member (online)');
        $this->addSql('DROP INDEX idx_online ON peer');
    }
}
