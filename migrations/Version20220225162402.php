<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220225162402 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add reasons to peer reviews';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE review ADD reasons LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\'');
        $this->addSql('UPDATE review SET reasons = \'[]\'');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE review DROP reasons');
    }
}
