<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Doctrine\Migrations\Exception\IrreversibleMigration;

final class Version20210130234743 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Update for multiple reviewers';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('DROP INDEX idx_peer ON review');
        $this->addSql('ALTER TABLE review ADD peers LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', ADD free_peers LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\'');
        $this->addSql('UPDATE review SET peers=concat(\'["\', peer, \'"]\'), free_peers=\'[]\'');
        $this->addSql('ALTER TABLE review DROP peer');
        $this->addSql('CREATE INDEX idx_peers ON review (peers)');
    }

    public function down(Schema $schema) : void
    {
        throw new IrreversibleMigration('Unable to reverse this migration');
    }
}
