<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210723092326 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Project table and relation to Peer';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE project (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, uri VARCHAR(255) NOT NULL, required_code_owners INT NOT NULL, required_others INT NOT NULL, priority INT NOT NULL, INDEX idx_uri (uri), INDEX idx_priority (priority), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE project_peer (project_id INT NOT NULL, peer_id VARCHAR(32) NOT NULL, INDEX IDX_9B7731A1166D1F9C (project_id), INDEX IDX_9B7731A120D91DB4 (peer_id), PRIMARY KEY(project_id, peer_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE project_peer ADD CONSTRAINT FK_9B7731A1166D1F9C FOREIGN KEY (project_id) REFERENCES project (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE project_peer ADD CONSTRAINT FK_9B7731A120D91DB4 FOREIGN KEY (peer_id) REFERENCES peer (id) ON DELETE CASCADE');

        $this->addSql("INSERT INTO `project` (`uri`, `name`, `required_code_owners`, `required_others`, `priority`) VALUES ('/', 'default', '0', '1', '-100')");
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE project_peer DROP FOREIGN KEY FK_9B7731A1166D1F9C');
        $this->addSql('DROP TABLE project');
        $this->addSql('DROP TABLE project_peer');
    }
}
