<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20201022222108 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Init';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('CREATE TABLE member (id INT AUTO_INCREMENT NOT NULL, peer VARCHAR(255) NOT NULL, code_group VARCHAR(255) NOT NULL, sort_order INT NOT NULL, online TINYINT(1) NOT NULL, INDEX idx_codegroup (code_group), INDEX idx_peer (peer), INDEX idx_sortorder (sort_order), INDEX idx_online (online), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE review (id INT AUTO_INCREMENT NOT NULL, peer VARCHAR(255) NOT NULL, author VARCHAR(255) NOT NULL, code_group VARCHAR(255) NOT NULL, url VARCHAR(255) DEFAULT NULL, content LONGTEXT NOT NULL, created DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', next_peer VARCHAR(255) NOT NULL, skipped_peer VARCHAR(255) DEFAULT NULL, INDEX idx_codegroup (code_group), INDEX idx_peer (peer), INDEX idx_author (author), INDEX idx_created (created), INDEX idx_url (url), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('DROP TABLE member');
        $this->addSql('DROP TABLE review');
    }
}
