<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220806230909 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Creating column for storing peer slack status based on emoji';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE peer ADD status VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE peer DROP status');
    }
}
