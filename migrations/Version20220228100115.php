<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220228100115 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'project requirements bound to code group';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE project ADD code_group VARCHAR(255) DEFAULT NULL');
        $this->addSql('CREATE INDEX idx_codegroup ON project (code_group)');
        $this->addSql("UPDATE project SET code_group = 'php'");
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP INDEX idx_codegroup ON project');
        $this->addSql('ALTER TABLE project DROP code_group');
    }
}
