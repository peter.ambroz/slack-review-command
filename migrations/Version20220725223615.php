<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220725223615 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Column for storing peer gitlab id';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE peer ADD gitlab_id INT DEFAULT NULL');
        $this->addSql('CREATE INDEX idx_gitlab_id ON peer (gitlab_id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP INDEX idx_gitlab_id ON peer');
        $this->addSql('ALTER TABLE peer DROP gitlab_id');
    }
}
