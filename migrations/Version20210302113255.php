<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210302113255 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Peer relation';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('CREATE TABLE peer (id VARCHAR(32) NOT NULL, full_name VARCHAR(255) DEFAULT NULL, default_group VARCHAR(255) DEFAULT NULL, home_updated DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', settings LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('INSERT INTO peer (id, full_name, settings) SELECT DISTINCT peer, full_name, "{}" FROM member');
        $this->addSql('DROP INDEX idx_peer ON member');
        $this->addSql('ALTER TABLE member CHANGE peer peer_id VARCHAR(32) NOT NULL, DROP full_name');
        $this->addSql('ALTER TABLE member ADD CONSTRAINT FK_70E4FA7820D91DB4 FOREIGN KEY (peer_id) REFERENCES peer (id)');
        $this->addSql('CREATE INDEX IDX_70E4FA7820D91DB4 ON member (peer_id)');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE member DROP FOREIGN KEY FK_70E4FA7820D91DB4');
        $this->addSql('DROP TABLE peer');
        $this->addSql('DROP INDEX IDX_70E4FA7820D91DB4 ON member');
        $this->addSql('ALTER TABLE member CHANGE peer_id peer VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, ADD full_name VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('CREATE INDEX idx_peer ON member (peer)');
    }
}
