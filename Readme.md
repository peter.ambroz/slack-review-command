### Running application

1. Execute command
   ```shell
   docker compose up -d
   ```
2. Done. Your application is now running on port `8080`.