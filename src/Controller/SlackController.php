<?php

declare(strict_types=1);

namespace App\Controller;

use App\EventSubscriber\RequestPayloadSubscriber;
use App\Form\SlackCommandType;
use App\Service\Handler\EventHandler;
use App\Service\Handler\InteractionHandler;
use App\Service\InteractionType;
use App\Service\ResponseQueue;
use App\Service\SlackCommandData;
use App\Service\SlackResponse;
use App\Service\SlackReview;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ServiceLocator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SlackController extends AbstractController
{
    public function __construct(private ServiceLocator $interactionLocator)
    {
    }

    /**
     * Slash (/) command action
     * Format: POST, url-encoded
     */

    #[Route(path: '/', name: 'slack_entrypoint')]
    public function command(
        Request $request,
        SlackReview $slack,
        ResponseQueue $queue,
        LoggerInterface $logger
    ): Response
    {
        $cmdData = new SlackCommandData();
        $cmdForm = $this->createForm(SlackCommandType::class, $cmdData);

        $cmdForm->submit($request->request->all());

        if ($cmdForm->isValid()) {
            if (!$this->isGranted('CHANNEL', $cmdData->getChannelId())) {
                return new SlackResponse('This command cannot be called in this channel');
            }

            if (!$slack->supports($cmdData)) {
                return new SlackResponse(sprintf("Sorry, I don't know how to run *%s*", $cmdData->getCommand()));
            }

            $logger->log(LogLevel::INFO, 'Command', [
                'command' => $cmdData->getCommand() . ' ' . $cmdData->getText(),
                'user' => $cmdData->getUserName() . '/' . $cmdData->getUserId(),
                'channel' => $cmdData->getChannelId()
            ]);

            $slackResponse = $slack->run($cmdData);

            // Handle response
            if ($slackResponse->isAsync()) {
                $slackResponse
                    ->setAsyncUrl($cmdData->getResponseUrl())
                    ->setTriggerId($cmdData->getTriggerId());

                $queue->enqueue($slackResponse);
                return new Response();
            }

            return $slackResponse;
        }
        return new SlackResponse('no data');
    }

    /**
     * Interaction entrypoint
     * Format: POST, url-encoded
     * There is just one input named "payload" which contains JSON object. This is being unpacked in a subscriber
     * @see RequestPayloadSubscriber
     */

    #[Route(path: '/interact', name: 'slack_interact')]
    public function interact(Request $request): Response
    {
        $type = InteractionType::tryFrom($request->request->get('type'));

        if ($type === null) {
            $type = InteractionType::UNKNOWN;
        }

        $this->getInteractionHandler($type)->handle($request);
        // interactions are always asynchronous and should return an empty immediate response
        return new Response();
    }

    /**
     * Event action
     * Format: POST, json
     * Unpacked in a subscriber
     * @see RequestPayloadSubscriber
     */

    #[Route(path: '/event', name: 'slack_event')]
    public function event(Request $request, EventHandler $eventHandler): Response
    {
        return $eventHandler->handle($request);
    }

    private function getInteractionHandler(InteractionType $type): InteractionHandler
    {
        return $this->interactionLocator->has($type->value)
            ? $this->interactionLocator->get($type->value)
            : $this->interactionLocator->get(InteractionType::UNKNOWN->value);
    }
}
