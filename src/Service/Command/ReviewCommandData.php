<?php

declare(strict_types=1);

namespace App\Service\Command;

use App\Entity\Peer;

class ReviewCommandData
{
    public function __construct(
        private string $operation,
        private ?string $group,
        private string $text,
        private Peer $user,
        private ?string $triggerId,
    ) {}

    public function getOperation(): string
    {
        return $this->operation;
    }

    public function getGroup(): ?string
    {
        return $this->group;
    }

    public function hasGroup(): bool
    {
        return $this->group !== null;
    }

    public function getText(): string
    {
        return $this->text;
    }

    public function getUser(): Peer
    {
        return $this->user;
    }

    public function getUserId(): string
    {
        return $this->user->getId();
    }

    public function getTriggerId(): ?string
    {
        return $this->triggerId;
    }
}
