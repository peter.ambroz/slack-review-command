<?php

declare(strict_types=1);

namespace App\Service\Command;

use App\Event\SetNextCommandExecutedEvent;
use App\Repository\ProjectRepository;
use App\Repository\ReviewRepository;
use App\Service\Exception\ReviewException;
use App\Service\ResponseType;
use App\Service\ReviewFactory;
use App\Service\ReviewRequirements;
use App\Service\SlackResponse;
use App\Service\SlackReview;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class SetNextCommand implements Command
{
    public function __construct(
        private ReviewRepository $reviewRepository,
        private ProjectRepository $projectRepository,
        private ReviewFactory $reviewFactory,
        private readonly EventDispatcherInterface $eventDispatcher,
    ) {}

    public static function getName(): string
    {
        return 'setNext';
    }

    public function run(ReviewCommandData $data): SlackResponse
    {
        $user = $data->getUser();
        if (!$data->hasGroup()) {
            $group = $user->getDefaultGroup();
            if ($group === null) {
                return new SlackResponse('You have no default group yet. Please call the command with an explicit group.');
            }
        } else {
            $group = $data->getGroup();
            if ($user->getDefaultGroup() === null) {
                $user->setDefaultGroup($group);
            }
        }

        $requirements = $this->parseRequirements($data->getText(), $group);

        if ($requirements->dynamicPeerCount < 0 || $requirements->dynamicPeerCount > 3) {
            return new SlackResponse('Too many peers required');
        }

        try {
            $nextReview = $this->reviewFactory->create($group, $user->getId(), $requirements);
        } catch (ReviewException $e) {
            return new SlackResponse('Unable to pick peers for review. Reason: ' . $e->getMessage());
        }

        $textWithoutStaticPeers = preg_replace('/<@[^>]+> ?/', '', $data->getText());
        $nextReview->setContent($textWithoutStaticPeers);
        $this->reviewRepository->add($nextReview);

        $this->eventDispatcher->dispatch(new SetNextCommandExecutedEvent($this, $nextReview));

        return new SlackResponse(
            sprintf(
                ":%s: %s %s [by <@%s>]",
                $group,
                SlackReview::tagPeersWithReasons($nextReview->getPeersWithReason()),
                $nextReview->getContent(),
                $data->getUserId()
            ),
            ResponseType::CHANNEL
        );
    }

    /**
     * Command format         -> amount of reviewers
     *
     * /review <URL>          -> dynamic:1, static:0
     * /review <URL> +1       -> dynamic:2, static:0
     * /review <URL> @name    -> dynamic:0, static:1
     * /review <URL> @name +1 -> dynamic:1, static:1
     */
    private function parseRequirements(string $text, string $codeGroup): ReviewRequirements
    {
        $reqs = new ReviewRequirements();

        if (preg_match('/ \+([0-9]+)/', $text, $matchedPlus) === 1) {
            $reqs->dynamicPeerCount = (int)$matchedPlus[1];
        }

        if (preg_match_all('/<@([^|>]+)[|>]/', $text, $matchedPeers) > 0) {
            $reqs->staticPeers = $matchedPeers[1];
            // having any static (explicit) peers overrides the url patterns
            return $reqs;
        }

        // merge requirements for projects
        if (preg_match_all('#<https?://[^>/]+(/[^>]+)>#', $text, $matchedUris) > 0) {
            $dynamicPeerCounts = [];

            foreach ($matchedUris[1] as $match) {
                $project = $this->projectRepository->matchUri($match, $codeGroup);
                $reqs->projects[] = $project;
                $dynamicPeerCounts[] = $project->getRequiredOthers();
            }

            $reqs->dynamicPeerCount += max($dynamicPeerCounts);
        }

        return $reqs;
    }
}
