<?php

declare(strict_types=1);

namespace App\Service\Command;

use App\Repository\MemberRepository;
use App\Service\Exception\ReviewException;
use App\Service\ReviewFactory;
use App\Service\ReviewRequirements;
use App\Service\SlackResponse;
use App\Service\SlackReview;

class ShowNextCommand implements Command
{
    public function __construct(
        private MemberRepository $memberRepository,
        private ReviewFactory $reviewFactory,
    ) {}

    public static function getName(): string
    {
        return 'showNext';
    }

    public function run(ReviewCommandData $data): SlackResponse
    {
        $me = $data->getUserId();
        if (!$data->hasGroup()) {
            $groups = $this->memberRepository->listGroups($me);
        } else {
            $groups = [$data->getGroup()];
        }

        $requirements = new ReviewRequirements();
        $requirements->dynamicPeerCount = 3;

        try {
            return new SlackResponse("Next 3 reviewers from your perspective:\n" .
                implode("\n", array_map(function (string $group) use ($me, $requirements) {
                    $nextReview = $this->reviewFactory->create($group, $me, $requirements, true);
                    return sprintf(":%s: *%s*: %s", $group, $group, SlackReview::tagPeers($nextReview->getPeers()));
                }, $groups)));
        } catch (ReviewException $e) {
            return new SlackResponse('Unable to find a peer for review. Reason: ' . $e->getMessage());
        }
    }
}
