<?php

declare(strict_types=1);

namespace App\Service\Command;

use App\Service\Gitlab\MergeRequestService;
use App\Service\SlackResponse;

class MergeRequestsCommand implements Command
{
    public function __construct(private readonly MergeRequestService $mergeRequestService)
    {
    }

    public function run(ReviewCommandData $data): SlackResponse
    {
        $mergeRequests = $this->mergeRequestService->getMergeRequestsToApprove($data->getUser());
        if (count($mergeRequests) === 0) {
            return new SlackResponse('Hooray! You don\'t have any merge requests to review.');
        }

        return new SlackResponse(text: '', blocks: $mergeRequests);
    }

    public static function getName(): string
    {
        return 'mr';
    }
}
