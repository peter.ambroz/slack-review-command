<?php

declare(strict_types=1);

namespace App\Service\Command;

use App\Entity\Project;
use App\Repository\ProjectRepository;
use App\Service\SlackResponse;

class CodeOwnersCommand implements Command
{
    public function __construct(private ProjectRepository $projectRepository)
    {
    }

    public static function getName(): string
    {
        return 'codeowners';
    }

    public function run(ReviewCommandData $data): SlackResponse
    {
        $text = implode("\n", array_map(function (Project $project) {
            if ($project->getRequiredCodeOwners() === 0) {
                return sprintf('*%s*: %d dev', $project->getName(), $project->getRequiredOthers());
            }

            $owners = implode(', ', $project->getPeers()->map(fn ($peer) => sprintf('<@%s>', $peer->getId()))->toArray());

            return sprintf(
                '*%s*: %d dev + %d of owners (%s)',
                $project->getName(),
                $project->getRequiredOthers(),
                $project->getRequiredCodeOwners(),
                $owners
            );
        }, $this->projectRepository->findBy([], ['id' => 'ASC'])));

        return new SlackResponse($text);
    }
}
