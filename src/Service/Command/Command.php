<?php

declare(strict_types=1);

namespace App\Service\Command;

use App\Service\SlackResponse;

interface Command
{
    public function run(ReviewCommandData $data): SlackResponse;

    public static function getName(): string;
}
