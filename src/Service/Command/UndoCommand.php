<?php

declare(strict_types=1);

namespace App\Service\Command;

use App\Repository\ReviewRepository;
use App\Service\ResponseType;
use App\Service\SlackResponse;
use App\Service\SlackReview;

class UndoCommand implements Command
{
    private const UNDO_TIME = 7200;

    public function __construct(
        private ReviewRepository $reviewRepository,
    ) {}

    public static function getName(): string
    {
        return 'undo';
    }

    public function run(ReviewCommandData $data): SlackResponse
    {
        $myLastReviews = $this->reviewRepository->getLatestReviewsByAuthor($data->getUserId());
        if (count($myLastReviews) === 0) {
            return new SlackResponse('You do not have any reviews yet.');
        }
        $myLastReview = $myLastReviews[0];

        $topReview = $this->reviewRepository->top($myLastReview->getCodeGroup());
        assert($topReview !== null);
        if ($topReview->getId() !== $myLastReview->getId()) {
            return new SlackResponse('Sorry, you can only use undo if *you* assigned the latest review.');
        }
        if ((new \DateTimeImmutable())->getTimestamp() - $myLastReview->getCreated()->getTimestamp() > self::UNDO_TIME) {
            return new SlackResponse('Review is too old. Maximum time for undo is ' . self::UNDO_TIME . ' seconds');
        }

        preg_match_all('/<@([^|>]+)[>|]/', $myLastReview->getContent(), $matchedPeers);
        $staticPeers = $matchedPeers[1];

        $this->reviewRepository->remove($myLastReview);
        return new SlackResponse(
            sprintf('%s, please ignore the latest review request.', SlackReview::tagPeers(array_merge($myLastReview->getPeers(), $staticPeers))),
            ResponseType::CHANNEL
        );
    }
}
