<?php

declare(strict_types=1);

namespace App\Service\Command;

use App\Repository\PeerRepository;

class OfflineCommand extends ToggleCommand
{
    public function __construct(
        protected PeerRepository $peerRepository,
    ) {
        parent::__construct($peerRepository, false);
    }

    public static function getName(): string
    {
        return 'offline';
    }
}
