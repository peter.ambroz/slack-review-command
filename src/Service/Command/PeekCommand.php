<?php

declare(strict_types=1);

namespace App\Service\Command;

use App\Entity\Review;
use App\Repository\ReviewRepository;
use App\Service\SlackResponse;
use App\Service\SlackReview;

class PeekCommand implements Command
{
    public function __construct(private ReviewRepository $reviewRepository)
    {
    }

    public static function getName(): string
    {
        return 'peek';
    }

    public function run(ReviewCommandData $data): SlackResponse
    {
        $me = $data->getUserId();
        $reviews = $this->reviewRepository->getLatestReviewsByAuthor($me, 3);

        if (count($reviews) === 0) {
            return new SlackResponse('You did not submit any review yet.');
        }

        return new SlackResponse(
            implode("\n", array_map(fn (Review $review) =>
                sprintf(
                    "*[%s]* :%s: %s %s [by <@%s>]",
                    $review->getCreated()->format('Y-m-d H:i'),
                    $review->getCodeGroup(),
                    SlackReview::tagPeersWithReasons($review->getPeersWithReason()),
                    $review->getContent(),
                    $review->getAuthor()
                ), $reviews))
        );
    }
}
