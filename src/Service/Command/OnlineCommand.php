<?php

declare(strict_types=1);

namespace App\Service\Command;

use App\Repository\PeerRepository;

class OnlineCommand extends ToggleCommand
{
    public function __construct(
        protected PeerRepository $peerRepository,
    ) {
        parent::__construct($peerRepository, true);
    }

    public static function getName(): string
    {
        return 'online';
    }
}
