<?php

declare(strict_types=1);

namespace App\Service\Command;

use App\Entity\Peer;
use App\Repository\MemberRepository;
use App\Repository\PeerRepository;
use App\Service\SlackResponse;

class ListCommand implements Command
{
    public function __construct(
        private MemberRepository $memberRepository,
        private PeerRepository $peerRepository,
    ) {}

    public static function getName(): string
    {
        return 'list';
    }

    public function run(ReviewCommandData $data): SlackResponse
    {
        if (!$data->hasGroup()) {
            $groups = $this->memberRepository->listGroups();
        } else {
            $groups = [$data->getGroup()];
        }

        $text = implode("\n", array_map(function (string $group) {
            $peers = $this->peerRepository->listPeers($group);
            return sprintf(':%s: *%s*: %s', $group, $group, implode(', ',
                    array_map(static function (Peer $peer) {
                        $taggedPeer = "<@$peer>";
                        if (!$peer->isOnline()) {
                            $taggedPeer = "($taggedPeer)";
                        }

                        return $taggedPeer;
                    }, $peers))
            );
        }, $groups));

        if (empty($groups)) {
            $text = 'There are no groups yet. Define the first one with *define* command!';
        }

        return new SlackResponse($text);
    }
}
