<?php

declare(strict_types=1);

namespace App\Service\Command;

use App\Service\SlackResponse;

class HelpCommand implements Command
{
    public function run(ReviewCommandData $data): SlackResponse
    {
        return new SlackResponse(self::getHelpText());
    }

    public static function getName(): string
    {
        return 'help';
    }

    public static function getHelpText(): string
    {
        return <<<EOF
*Code review automatic assigner*
*Usage:*
Quick assign MR to next available reviewers in your code group (auto-pick by project requirements):
```/review https://gitlab/request thanks
/review <any text starting with URL>```
Advanced choice of reviewers:
`/review https://example.com +1` yields *2* dynamic reviewers.
`/review @john.doe https://example.com` selects the *1* mentioned reviewer only. More names are allowed.
`/review @john.doe https://example.com +1` yields *2* reviewers.
Note: Mentioning any specific developer overrides the project requirements.

Assign MR to the next reviewer in a specific group (i.e. you are in more than 1)
```/review php https://gitlab/request thanks
/review [group] <any text>```
Check who is next from your subjective perspective (no assignment is made):
```/review
/review [group]```
Undo the last assignment - in case you screwed up:
```/review undo```
See the objective list of reviewers in all groups or in a specific group:
```/review list
/review list [group]```
Define the new order for code reviews / create new group. Be sure to include the group name. Put offline people in parentheses:
```/review define php @john.doe @randall.munroe
/review define <group> [comma- / space- separated list of slack users]```
Going on vacation? Mark yourself as offline. Someone already away? Mark them as well:
```/review offline
/review offline php @bill.gates
/review [offline | online] [group] [user list]```
See the project requirements
```/review codeowners```
See up to 3 latest reviews that you requested. The response is only seen by you - feel free to copy/paste the parts as needed
```/review peek```
Get a list of merge requests you should review. The response is only seen by you.
```/review mr```

*Reference:*
```/review [undo | list | define | offline | online | codeowners | peek | help | mr] [group] [text | user list]```
  If you omit the command and the text is non-empty, the next person is assigned for the code review.
  Omitting both the command and text will show you the next persons without assigning them (dry run).
  Group is mandatory for 'define' operation.
  As a protection against typos, the "Quick assign" syntax checks whether the text starts with an URL.
EOF;
    }
}
