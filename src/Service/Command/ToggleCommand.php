<?php

declare(strict_types=1);

namespace App\Service\Command;

use App\Repository\PeerRepository;
use App\Service\ResponseType;
use App\Service\SlackResponse;
use App\Service\SlackReview;

abstract class ToggleCommand implements Command
{
    public function __construct(
        protected PeerRepository $peerRepository,
        protected bool $online,
    ) {}

    public function run(ReviewCommandData $data): SlackResponse
    {
        $changedPeers = [];
        if (empty($data->getText())) {
            $this->peerRepository->toggle($data->getUserId(), $this->online);
            $changedPeers[] = $data->getUserId();
        } else {
            foreach ($this->peerRepository->parsePeerList($data->getText()) as $peer) {
                $this->peerRepository->toggle($peer->getId(), $this->online);
                $changedPeers[] = $peer->getId();
            }
        }

        $self = (count($changedPeers) === 1 && $changedPeers[0] === $data->getUserId());

        return new SlackResponse(
            sprintf('%s %s now %s' . ($self ? '' : ' [by <@' . $data->getUserId() . '>]'),
                SlackReview::tagPeers($changedPeers), ((count($changedPeers) > 1) ? 'are' : 'is'),
                $this->online ? 'online' : 'offline'),
            ResponseType::CHANNEL
        );
    }
}
