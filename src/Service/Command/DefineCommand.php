<?php

declare(strict_types=1);

namespace App\Service\Command;

use App\Entity\Member;
use App\Repository\MemberRepository;
use App\Repository\PeerRepository;
use App\Service\ResponseType;
use App\Service\SlackApi\SlackApi;
use App\Service\SlackApi\SlackApiException;
use App\Service\SlackApi\SlackMethod;
use App\Service\SlackResponse;

class DefineCommand implements Command
{
    public function __construct(
        private MemberRepository $memberRepository,
        private PeerRepository $peerRepository,
        private ListCommand $listCommand,
        private SlackApi $slackApi,
    ) {}

    public static function getName(): string
    {
        return 'define';
    }

    public function run(ReviewCommandData $data): SlackResponse
    {
        if (!$data->hasGroup()) {
            return new SlackResponse('Please specify the group name!');
        }

        $newPeers = $this->peerRepository->parsePeerList($data->getText(), $data->getGroup());
        $this->memberRepository->dropMembers($data->getGroup());

        foreach ($newPeers as $i => $peer) {
            if ($peer->getFullName() === null) {
                try {
                    $response = $this->slackApi->get(SlackMethod::USERS_INFO, ['user' => $peer->getId()]);
                    $peer->setFullName($response['user']['real_name']);
                } catch (SlackApiException) {
                }
            }

            $member = (new Member())
                ->setPeer($peer)
                ->setSortOrder($i)
                ->setCodeGroup($data->getGroup());

            $this->memberRepository->add($member);
        }
        $this->memberRepository->flush();

        $listData = new ReviewCommandData('list', $data->getGroup(), '', $data->getUser(), $data->getTriggerId());
        $listResponse = $this->listCommand->run($listData);
        return new SlackResponse(
            sprintf("The *%s* group order was changed by <@%s>\n%s", $data->getGroup(), $data->getUserId(), $listResponse->getData()['text']),
            ResponseType::CHANNEL
        );
    }
}
