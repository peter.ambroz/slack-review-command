<?php

declare(strict_types=1);

namespace App\Service;

enum ResponseType: string
{
    case PRIVATE = 'ephemeral';
    case CHANNEL = 'in_channel';
    case MODAL = 'modal';
}
