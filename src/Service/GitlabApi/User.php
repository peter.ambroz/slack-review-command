<?php

declare(strict_types=1);

namespace App\Service\GitlabApi;

final class User
{
    public function __construct(
        private readonly int $id,
        private readonly string $name,
        private readonly string $username,
        private readonly string $state,
        private readonly string $avatarUrl,
        private readonly string $webUrl
    ) {
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function getState(): string
    {
        return $this->state;
    }

    public function getAvatarUrl(): string
    {
        return $this->avatarUrl;
    }

    public function getWebUrl(): string
    {
        return $this->webUrl;
    }
}
