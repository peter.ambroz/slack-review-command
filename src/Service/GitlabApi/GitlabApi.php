<?php

declare(strict_types=1);

namespace App\Service\GitlabApi;

use App\Service\GitlabApi\MergeRequest\MergeRequest;
use App\Service\GitlabApi\MergeRequest\MergeRequestApprovals;
use App\Service\GitlabApi\MergeRequest\MergeRequestFactory;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

final class GitlabApi
{
    public function __construct(private readonly HttpClientInterface $gitlabApiClient)
    {
    }

    /**
     * @param int[] $reviewerIds
     * @throws TransportExceptionInterface
     */
    public function assignReviewers(string $uri, array $reviewerIds): void
    {
        $this->gitlabApiClient->request(
            'PUT',
            sprintf(
                'projects/%s/merge_requests/%s',
                urlencode($this->getProjectPath($uri)),
                $this->getMergeRequestId($uri)
            ),
            [
                'json' => ['reviewer_ids' => $reviewerIds],
            ]
        );
    }

    /**
     * @return MergeRequest[]
     */
    public function getMergeRequests(int $userId): array
    {
        $response = $this->gitlabApiClient->request(
            'GET',
            'merge_requests?' . http_build_query(['state' => 'opened', 'scope' => 'all', 'reviewer_id' => $userId]),
        );

        $mergeRequests = [];

        foreach ($response->toArray() as $mergeRequest) {
            $mergeRequests[] = MergeRequestFactory::createMergeRequest($mergeRequest);
        }

        return $mergeRequests;
    }

    public function getMergeRequestApprovals(int $projectId, int $mergeRequestId): MergeRequestApprovals
    {
        $response = $this->gitlabApiClient->request(
            'GET',
            sprintf('projects/%s/merge_requests/%s/approvals', $projectId, $mergeRequestId),
        );
        return MergeRequestFactory::createMergeRequestApprovals($response->toArray());
    }

    private function getProjectPath(string $uri): string
    {
        preg_match('/(gitlab\.com|git\.websupport\.sk)\/(.+)(?=-)/', $uri, $matches);
        if (empty($matches[2])) {
            throw new GitlabException('Couldn\'t get project path');
        }
        return trim($matches[2], '/');
    }

    private function getMergeRequestId(string $uri): int
    {
        preg_match('/merge_requests\/(\d+)/', $uri, $matches);
        if (empty($matches[1])) {
            throw new GitlabException('Couldn\'t get merge request ID');
        }
        return (int) $matches[1];
    }
}
