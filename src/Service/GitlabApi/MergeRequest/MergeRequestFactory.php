<?php

declare(strict_types=1);

namespace App\Service\GitlabApi\MergeRequest;

use App\Service\GitlabApi\UserFactory;

class MergeRequestFactory
{
    public static function createMergeRequest(array $data): MergeRequest
    {
        return new MergeRequest(
            $data['id'],
            $data['iid'],
            $data['project_id'],
            $data['web_url'],
            UserFactory::createUser($data['author']),
            $data['title']
        );
    }

    public static function createMergeRequestApprovals(array $data): MergeRequestApprovals
    {
        $users = [];

        foreach($data['approved_by'] as $approvedBy) {
            $users[] = UserFactory::createUser($approvedBy['user']);
        }

        return new MergeRequestApprovals(
            $data['approvals_required'],
            $data['approvals_left'],
            $users,
        );
    }
}
