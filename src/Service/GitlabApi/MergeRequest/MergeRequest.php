<?php

declare(strict_types=1);

namespace App\Service\GitlabApi\MergeRequest;

use App\Service\GitlabApi\User;

class MergeRequest
{
   public function __construct(
       private readonly int $id,
       private readonly int $mergeRequestId,
       private readonly int $projectId,
       private readonly string $webUrl,
       private readonly User $author,
       private readonly string $title,
   ) {
   }

    public function getId(): int
    {
        return $this->id;
    }

    public function getMergeRequestId(): int
    {
        return $this->mergeRequestId;
    }

    public function getProjectId(): int
    {
        return $this->projectId;
    }

    public function getWebUrl(): string
    {
        return $this->webUrl;
    }

    public function getAuthor(): User
    {
        return $this->author;
    }

    public function getTitle(): string
    {
        return $this->title;
    }
}
