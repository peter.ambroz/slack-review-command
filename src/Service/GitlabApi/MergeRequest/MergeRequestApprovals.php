<?php

declare(strict_types=1);

namespace App\Service\GitlabApi\MergeRequest;

use App\Service\GitlabApi\User;

class MergeRequestApprovals
{
    public function __construct(
        private readonly int $approvalsRequired,
        private readonly int $approvalsLeft,
        private readonly array $approvedBy
    )
    {
    }

    public function getApprovalsRequired(): int
    {
        return $this->approvalsRequired;
    }

    public function getApprovalsLeft(): int
    {
        return $this->approvalsLeft;
    }

    /**
     * @return User[]
     */
    public function getApprovedBy(): array
    {
        return $this->approvedBy;
    }

    public function isApprovedByUser(int $userId): bool
    {
        return count(array_filter($this->approvedBy, fn (User $user) => $user->getId() === $userId)) > 0;
    }
}
