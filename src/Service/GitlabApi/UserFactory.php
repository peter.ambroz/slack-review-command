<?php

declare(strict_types=1);

namespace App\Service\GitlabApi;

class UserFactory
{
    public static function createUser(array $data): User
    {
        return new User(
            $data['id'],
            $data['name'],
            $data['username'],
            $data['state'],
            $data['avatar_url'],
            $data['web_url'],
        );
    }
}
