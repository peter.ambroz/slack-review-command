<?php

declare(strict_types=1);

namespace App\Service\GitlabApi;

use RuntimeException;

class GitlabException extends RuntimeException
{

}
