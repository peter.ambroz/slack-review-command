<?php

declare(strict_types=1);

namespace App\Service;

enum InteractionType: string
{
    case WORKFLOW_STEP_EDIT = 'workflow_step_edit';
    case VIEW_SUBMISSION = 'view_submission';
    case UNKNOWN = '__unknown';
}
