<?php

declare(strict_types=1);

namespace App\Service\SlackHome;

class BlockKit
{
    public static function home(string $id, array $blocks): array
    {
        return [
            'type' => 'home',
            'callback_id' => $id,
            'blocks' => $blocks,
        ];
    }

    public static function plainText(string $text, bool $emoji = true): array
    {
        return [
            'type' => 'plain_text',
            'text' => $text,
            'emoji' => $emoji,
        ];
    }

    public static function markdown(string $text): array
    {
        return [
            'type' => 'mrkdwn',
            'text' => $text,
        ];
    }

    public static function section(array $textBlock): array
    {
        return [
            'type' => 'section',
            'text' => $textBlock,
        ];
    }

    public static function header(array $textBlock): array
    {
        return [
            'type' => 'header',
            'text' => $textBlock,
        ];
    }

    public static function divider(): array
    {
        return ['type' => 'divider'];
    }

    public static function element(
        string $type,
        string $id,
        ?string $placeholder = null,
        array $options = []
    ): array
    {
        $data = array_merge([
            'type' => $type,
            'action_id' => $id,
        ], $options);

        if ($placeholder !== null) {
            $data['placeholder'] = self::plainText($placeholder);
        }

        return $data;
    }

    public static function input(
        string $type,
        string $id,
        string $label,
        ?string $placeholder = null,
        array $options = []
    ): array
    {
        return [
            'type' => 'input',
            'element' => self::element($type, $id, $placeholder, $options),
            'label' => self::plainText($label),
        ];
    }

    public static function actions(string $id, array $elements): array
    {
        return [
            'type' => 'actions',
            'block_id' => $id,
            'elements' => $elements,
        ];
    }

    public static function option(array $textBlock, string $value): array
    {
        return [
            'text' => $textBlock,
            'value' => $value,
        ];
    }

    public static function confirm(string $title, string $text, string $confirm = 'Confirm', string $cancel = 'Go back'): array
    {
        return [
            'title' => self::plainText($title),
            'text' => self::markdown($text),
            'confirm' => self::plainText($confirm),
            'deny' => self::plainText($cancel),
        ];
    }
}
