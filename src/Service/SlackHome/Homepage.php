<?php

declare(strict_types=1);

namespace App\Service\SlackHome;

use App\Entity\Peer;
use App\Entity\Review;
use App\Repository\PeerRepository;
use App\Repository\ReviewRepository;
use App\Service\Command\HelpCommand;

class Homepage
{
    public function __construct(
        private ReviewRepository $reviewRepository,
        private PeerRepository $peerRepository,
    ) {}

    public function render(Peer $owner): array
    {
        $homePage = BlockKit::home('homepage', [
            BlockKit::header(
                BlockKit::plainText('Code review help'),
            ),
            BlockKit::section(
                BlockKit::markdown(HelpCommand::getHelpText()),
            ),
        ]);

        $homePage['blocks'] = array_merge($homePage['blocks'], $this->debugInfo($owner));

        return $homePage;
    }

    private function debugInfo(Peer $owner): array
    {
        $group = $owner->getDefaultGroup() ?? 'php';

        $peers = $this->peerRepository->listPeers($group);
        $review = $this->reviewRepository->top($group);
        if ($review === null) {
            $review = (new Review()) // sentinel
                ->setAuthor('')
                ->setCodeGroup($group)
                ->setNextPeer($peers[0]->getId() ?? '');
        }

        $memberInfo = [];
        foreach ($peers as $peer) {
            $peerId = $peer->getId();
            $name = $peer->getFullName();
            $offPtr = '';
            $nextPtr = '';
            $skipPtr = '';
            $freePtr = '';
            if (!$peer->isOnline()) {
                $name = sprintf('(%s)', $name);
                $offPtr = ' :palm_tree: offline';
            }
            if ($review->getNextPeer() === $peerId) {
                $name = sprintf('*%s*', $name);
                $nextPtr = ' :arrow_left: next';
            }
            if ($review->getSkippedPeer() === $peerId) {
                $skipPtr = ' :man-shrugging: skipped';
            }
            if (in_array($peerId, $review->getFreePeers())) {
                $freePtr = ' :sleeping_accommodation: free';
            }

            $memberInfo[] = sprintf('%s%s%s%s%s', $name, $nextPtr, $skipPtr, $freePtr, $offPtr);
        }

        return [
            BlockKit::divider(),
            BlockKit::section(
                BlockKit::markdown('*Information for ' . $group . ' group*')
            ),
            BlockKit::section(
                BlockKit::markdown(implode("\n", $memberInfo))
            ),
        ];
    }
}
