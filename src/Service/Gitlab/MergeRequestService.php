<?php

declare(strict_types=1);

namespace App\Service\Gitlab;

use App\Entity\Peer;
use App\Repository\PeerRepository;
use App\Service\GitlabApi\GitlabApi;
use App\Service\SlackHome\BlockKit;
use DateTime;

class MergeRequestService
{
    public function __construct(private readonly PeerRepository $peerRepository, private readonly GitlabApi $gitlabApi)
    {
    }

    public function getMergeRequestsToApprove(Peer $peer): array
    {
        $blocks = [];
        $mergeRequests = $this->gitlabApi->getMergeRequests($peer->getGitlabId());

        foreach ($mergeRequests as $mergeRequest) {
            $approvals = $this->gitlabApi->getMergeRequestApprovals(
                $mergeRequest->getProjectId(),
                $mergeRequest->getMergeRequestId()
            );
            if ($approvals->isApprovedByUser($peer->getGitlabId())) {
                continue;
            }
            $authorSlackId = $this->convertGitlabIdToSlackId($mergeRequest->getAuthor()->getId());
            $mergeRequestText = BlockKit::markdown(sprintf(
                'Merge request: <%s|%s> by %s. Approvals %d/%d',
                $mergeRequest->getWebUrl(),
                $mergeRequest->getTitle(),
                $authorSlackId ? sprintf('<@%s>', $authorSlackId) : $mergeRequest->getAuthor()->getName(),
                $approvals->getApprovalsRequired() - $approvals->getApprovalsLeft(),
                $approvals->getApprovalsRequired()
            ));
            $blocks[] = BlockKit::section($mergeRequestText);
        }

        if (count($blocks) === 0) {
            return [];
        }

        array_unshift($blocks, $this->generateGreeting($peer->getFullName()));
        $blocks[] = $this->generateEncouragement();

        return $blocks;
    }

    private function convertGitlabIdToSlackId(int $gitlabId): ?string
    {
        return $this->peerRepository->findOneBy(['gitlabId' => $gitlabId])?->getId();
    }

    private function parseFirstName(string $name): string
    {
        return explode(' ', $name)[0];
    }

    private function generateGreeting(string $fullName): array
    {
        $partOfDay = $this->getPartOfDay();

        $message = match ($partOfDay) {
            'morning' => "Good morning %s :wave:\nHere are merge requests you should take a look at today:",
            'afternoon' => "Good afternoon %s :wave:\nHere are merge requests you should take a look at today:",
            'evening' => "Good evening %s :yawning_face:\nHere are merge requests you should take a look at:",
            'night' => "Are you still working %s? :bed:\nHere are merge requests you should take a look at: (or maybe you can take a look at them tomorrow since no one is working now anyway :shrug:)",
        };

        $greeting = BlockKit::markdown(
            sprintf(
                $message,
                $this->parseFirstName($fullName)
            )
        );
        return BlockKit::section($greeting);
    }

    private function generateEncouragement(): array
    {
        $appropriateEmojis = ['muscle', 'the_horns', '+1', 'crossed_fingers'];
        $encouragement = BlockKit::markdown(
            sprintf("Have a productive day! :%s:", $appropriateEmojis[array_rand($appropriateEmojis)])
        );
        return BlockKit::section($encouragement);
    }

    private function getPartOfDay(): string
    {
        $currentHour = (int) (new DateTime())->format('G');

        if ($currentHour >= 5 && $currentHour < 12) {
            $partOfDay = 'morning';
        } elseif ($currentHour >= 12 && $currentHour < 18) {
            $partOfDay = 'afternoon';
        } elseif ($currentHour >= 18 && $currentHour < 20) {
            $partOfDay = 'evening';
        } else {
            $partOfDay = 'night';
        }

        return $partOfDay;
    }
}
