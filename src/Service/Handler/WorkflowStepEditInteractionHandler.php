<?php

declare(strict_types=1);

namespace App\Service\Handler;

use App\Service\InteractionType;
use App\Service\SlackApi\SlackApi;
use App\Service\SlackApi\SlackMethod;
use App\Service\SlackHome\BlockKit;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;

class WorkflowStepEditInteractionHandler implements InteractionHandler
{
    public function __construct(
        private LoggerInterface $logger,
        private SlackApi $slackApi,
    ) {}

    public static function getType(): string
    {
        return InteractionType::WORKFLOW_STEP_EDIT->value;
    }

    public function handle(Request $request): void
    {
        $triggerId = $request->request->get('trigger_id');
        $this->logger->info('Triggering view open', ['trigger_id' => $triggerId]);

        $response = $this->slackApi->post(SlackMethod::VIEWS_OPEN, [
            'trigger_id' => $triggerId,
            'view' => [
                'type' => 'workflow_step',
                'blocks' => [
                    BlockKit::section(
                        BlockKit::markdown('Nothing to configure. Just submit.')
                    ),
                ],
            ],
        ]);

        $this->logger->info('View open response', $response);
    }
}
