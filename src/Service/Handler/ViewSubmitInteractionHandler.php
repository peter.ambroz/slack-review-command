<?php

declare(strict_types=1);

namespace App\Service\Handler;

use App\Service\InteractionType;
use App\Service\SlackApi\SlackApi;
use App\Service\SlackApi\SlackMethod;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;

class ViewSubmitInteractionHandler implements InteractionHandler
{
    public function __construct(
        private LoggerInterface $logger,
        private SlackApi $slackApi,
    ) {}

    public static function getType(): string
    {
        return InteractionType::VIEW_SUBMISSION->value;
    }

    public function handle(Request $request): void
    {
        $step = $request->request->all('workflow_step');
        $this->logger->info('ViewSubmission update step', $step);

        $response = $this->slackApi->post(SlackMethod::WORKFLOWS_UPDATE_STEP, [
            'workflow_step_edit_id' => $step['workflow_step_edit_id'],
        ]);

        $this->logger->info('Workflow step update response', $response);
    }
}
