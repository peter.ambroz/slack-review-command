<?php

declare(strict_types=1);

namespace App\Service\Handler\EventHandler;

use App\Entity\Peer;
use App\Repository\PeerRepository;
use App\Service\SlackApi\SlackApi;
use App\Service\SlackApi\SlackMethod;
use App\Service\SlackHome\Homepage;

class AppHomeOpenedHandler implements EventHandlerInterface
{
    public function __construct(
        private readonly SlackApi $slackApi,
        private readonly Homepage $homepage,
        private readonly PeerRepository $peerRepository,
    ) {
    }

    public function handle(array $event): void
    {
        $peer = $this->peerRepository->findOrCreate($event['user']);
        $this->slackApi->post(SlackMethod::VIEWS_PUBLISH, [
            'user_id' => $event['user'],
            'view' => $this->homepage->render($peer),
        ]);

        $peer->setHomeUpdated(new \DateTimeImmutable());
        $peer->addSetting(Peer::REVIEW_REMINDER, 1);
        $this->peerRepository->flush();
    }

    public static function getType(): string
    {
        return EventType::APP_HOME_OPENED->value;
    }
}
