<?php

declare(strict_types=1);

namespace App\Service\Handler\EventHandler;

enum EventType: string
{
    case USER_STATUS_CHANGED = 'user_status_changed';
    case APP_HOME_OPENED = 'app_home_opened';
}
