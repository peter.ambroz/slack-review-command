<?php

declare(strict_types=1);

namespace App\Service\Handler\EventHandler;

interface EventHandlerInterface
{
    public function handle(array $event): void;

    public static function getType(): string;
}
