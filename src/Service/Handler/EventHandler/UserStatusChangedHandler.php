<?php

declare(strict_types=1);

namespace App\Service\Handler\EventHandler;

use App\Entity\Status;
use App\Repository\PeerRepository;
use Exception;

class UserStatusChangedHandler implements EventHandlerInterface
{
    public function __construct(private readonly PeerRepository $peerRepository)
    {}

    public function handle(array $event): void
    {
        $peer = $this->peerRepository->findOneBy(['id' => $event['user']['id']]);

        if (!$peer) {
            return;
        }

        try {
            $eventStatus = $this->convertEmojiToStatus($event['user']['profile']['status_emoji']);
        } catch (Exception) {
            return;
        }

        $peer->setStatus($eventStatus);
        $this->peerRepository->flush();
    }

    public static function getType(): string
    {
        return EventType::USER_STATUS_CHANGED->value;
    }

    /**
     * @throws Exception
     */
    private function convertEmojiToStatus(string $emoji): ?Status
    {
        $emoji = trim($emoji, ':');
        return match ($emoji) {
            'palm_tree' => Status::VACATION,
            'face_with_thermometer' => Status::SICK,
            '' => null,
            default => throw new Exception('Unsupported emoji')
        };
    }
}
