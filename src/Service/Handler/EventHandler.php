<?php

declare(strict_types=1);

namespace App\Service\Handler;

use App\Service\Handler\EventHandler\EventHandlerInterface;
use App\Service\Handler\EventHandler\EventType;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ServiceLocator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use ValueError;

class EventHandler
{
    private const URL_VERIFICATION = 'url_verification';
    private const EVENT_CALLBACK = 'event_callback';

    public function __construct(
        private readonly LoggerInterface $logger,
        private readonly ServiceLocator $eventHandlers
    ) {}

    public function handle(Request $request): Response
    {
        $this->logger->info('Event', [
            'request' => $request->request->all(),
        ]);

        $type = $request->request->get('type');

        if ($type === self::URL_VERIFICATION) {
            return $this->handleUrlVerification($request);
        } elseif ($type !== self::EVENT_CALLBACK) {
            return new Response();
        }

        $this->handleInnerEvent($request);

        return new Response();
    }

    private function handleUrlVerification(Request $request): Response
    {
        $challenge = $request->request->get('challenge');
        return new Response($challenge);
    }

    private function handleInnerEvent(Request $request): void
    {
        $event = $request->request->all('event');
        try {
            $eventType = EventType::from($event['type']);
        } catch (ValueError) {
            $this->logger->warning(sprintf('Unsupported event type: %s', $event['type']));
            return;
        }

        if (!$this->eventHandlers->has($eventType->value)) {
            $this->logger->warning(sprintf('Handler for event: %s is not implemented :(', $eventType->value));
            return;
        }

        /** @var EventHandlerInterface $handler */
        $handler = $this->eventHandlers->get($eventType->value);
        $handler->handle($event);
    }
}
