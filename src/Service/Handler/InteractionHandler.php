<?php

declare(strict_types=1);

namespace App\Service\Handler;

use Symfony\Component\HttpFoundation\Request;

interface InteractionHandler
{
    public function handle(Request $request): void;

    public static function getType(): string;
}
