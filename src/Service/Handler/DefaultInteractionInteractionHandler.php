<?php

declare(strict_types=1);

namespace App\Service\Handler;

use App\Service\InteractionType;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;
use Symfony\Component\HttpFoundation\Request;

class DefaultInteractionInteractionHandler implements InteractionHandler
{
    public function __construct(
        private LoggerInterface $logger,
    ) {}

    public static function getType(): string
    {
        return InteractionType::UNKNOWN->value;
    }

    public function handle(Request $request): void
    {
        $this->logger->log(LogLevel::INFO, 'Interaction', $request->request->all());
    }
}
