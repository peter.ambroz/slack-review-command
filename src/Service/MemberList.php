<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Peer;
use App\Repository\PeerRepository;
use App\Service\Exception\ReviewException;

class MemberList
{
    /** @var Peer[] */
    private array $list;

    private int $current;

    private int $size;

    public function __construct(PeerRepository $peerRepository, string $group)
    {
        $this->list = $peerRepository->listPeers($group);
        $this->size = count($this->list);
        $this->current = 0;
    }

    public function rewind(string $peerId): bool
    {
        foreach ($this->list as $key => $peer) {
            if ($peer->getId() === $peerId) {
                $this->current = $key;
                return true;
            }
        }

        return false;
    }

    /**
     * @throws ReviewException
     */
    public function get(): Peer
    {
        if ($this->size === 0) {
            throw new ReviewException('Empty peer list');
        }

        $peer = $this->list[$this->current];
        $this->current = ($this->current + 1) % $this->size;

        return $peer;
    }

    /**
     * @throws ReviewException
     */
    public function getOnline(): Peer
    {
        for ($i = 0; $i < $this->size; $i++) {
            $peer = $this->get();
            if ($peer->isOnline()) {
                return $peer;
            }
        }

        throw new ReviewException('No online members');
    }

    public function isPeerOnline(string $peerId): bool
    {
        foreach ($this->list as $peer) {
            if ($peer->isOnline() && $peer->getId() === $peerId) {
                return true;
            }
        }

        return false;
    }

    public function has(string $peerId): bool
    {
        foreach ($this->list as $peer) {
            if ($peer->getId() === $peerId) {
                return true;
            }
        }

        return false;
    }
}
