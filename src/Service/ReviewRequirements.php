<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Project;

class ReviewRequirements
{
    /**
     * how many peers to assign by default from the queue
     */
    public int $dynamicPeerCount = 0;

    /**
     * these will be assigned on top of the dynamicPeerCount
     * @var string[]
     */
    public array $staticPeers = [];

    /**
     * @var Project[]
     */
    public array $projects = [];
}
