<?php

declare(strict_types=1);

namespace App\Service;

class ResponseQueue
{
    /** @var SlackResponse[] */
    private array $queue = [];

    public function enqueue(SlackResponse $response)
    {
        $this->queue[] = $response;
    }

    public function dequeue(): ?SlackResponse
    {
        return array_shift($this->queue);
    }

    public function count(): int
    {
        return count($this->queue);
    }
}
