<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\ReasonForPeer;
use App\Repository\MemberRepository;
use App\Repository\PeerRepository;
use App\Service\Command\Command;
use App\Service\Command\ReviewCommandData;
use Symfony\Component\DependencyInjection\ServiceLocator;

class SlackReview
{
    public function __construct(
        private ServiceLocator $commandLocator,
        private MemberRepository $memberRepository,
        private PeerRepository $peerRepository,
    ) {}

    public function supports(SlackCommandData $data): bool
    {
        return in_array($data->getCommand(), ['/review', '/test-review'], true);
    }

    public function run(SlackCommandData $data): SlackResponse
    {
        $operations = ['help', 'list', 'define', 'undo', 'online', 'offline', 'codeowners', 'peek', 'mr'];

        $parts = explode(' ', $data->getText());

        $operation = null;
        if (in_array(strtolower($parts[0]), $operations)) {
            $operation = strtolower(array_shift($parts));
        }

        $group = null;
        if (count($parts)) {
            $groups = $this->memberRepository->listGroups();
            // Group doesn't have to exist if it is going to be defined
            if (in_array($parts[0], $groups) || $operation === 'define' || $operation === 'list') {
                $group = array_shift($parts);
            }
        }

        $text = implode(' ', $parts);

        // guard some obvious typos by enforcing an URL or a person reference at the start
        if ($operation === null
            && $group === null
            && !empty($text)
            && !str_starts_with($text, '<http')
            && !str_starts_with($text, '<@')
        ) {
            return new SlackResponse('Please start the review text with an URL.');
        }

        if ($operation === null) {
            if (empty($text)) {
                $operation = 'showNext';
            } else {
                $operation = 'setNext';
            }
        }

        $user = $this->peerRepository->findOrCreate($data->getUserId(), $group);
        $reviewData = new ReviewCommandData($operation, $group, $text, $user, $data->getTriggerId());

        return $this->getCommand($operation)->run($reviewData);
    }

    /**
     * Convert an array of peer ids into a printable string with highlighted names
     *
     * @param string[] $peers
     */
    public static function tagPeers(array $peers): string
    {
        return implode(', ', array_map(fn (string $x): string => sprintf('<@%s>', $x), $peers));
    }

    /**
     * @param ReasonForPeer[] $peers
     */
    public static function tagPeersWithReasons(array $peers): string
    {
        return implode(', ', array_map(
            fn (ReasonForPeer $x): string =>
                ($x->getReasonText() !== ''
                    ? sprintf('[%s]: <@%s>', $x->getReasonText(), $x->getPeerId())
                    : sprintf('<@%s>', $x->getPeerId())
                ),
            $peers
        ));
    }

    private function getCommand(string $operation): Command
    {
        return $this->commandLocator->has($operation)
            ? $this->commandLocator->get($operation)
            : $this->commandLocator->get('help');
    }
}
