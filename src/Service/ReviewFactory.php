<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Peer;
use App\Entity\Project;
use App\Entity\ReasonForPeer;
use App\Entity\Review;
use App\Repository\PeerRepository;
use App\Repository\ReviewRepository;
use App\Service\Exception\ReviewException;
use Psr\Log\LoggerInterface;

class ReviewFactory
{
    public function __construct(
        private ReviewRepository $reviewRepository,
        private PeerRepository $peerRepository,
        private LoggerInterface $logger,
    ) {
    }

    /**
     * Original algorithm:
     * 1) Take the member LIST for a given group. Make sure it is not empty and that there is at least one online person other than you.
     * 2) Take the LAST REVIEW. If there is none, just pretend there is one, designating the first person from the LIST the next reviewer.
     * 3) NEXT REVIEWER is either NextPeer from LAST REVIEW or the first online person in the LIST coming after them. Is it you? Go to (4). Is there a SkippedPeer? Go to (5). Otherwise (DONE)
     * 4) You are the NEXT REVIEWER so try the next person from the member list. (DONE)
     * 5) Make the SkippedPeer as the NEXT REVIEWER. Is the person offline or no longer in the list? Go to (3) and pretend there was no SkippedPeer. Is it you? Go to (6). Otherwise (DONE)
     * 6) You keep being skipped. For now try the next online person from the member list. (DONE)
     *
     * Dynamic peers are the ones we do not know in advance and select them by means of adding "+N" to the request. By default we request one dynamic peer.
     * Static peers are mentioned explicitly in the request and are taken regardless their online status and they are guaranteed not to appear in the dynamic list.
     * Every static peer which is also a member of the group will receive one free round - they are doing the out-of-order code review and deserve some rest later on.
     *
     * There is a very important part in the selection algorithm called a Free For This Round Peer List.
     * This list associates the amount of "free" rounds to a peer. We start by loading the free peers from DB and giving them one free round each.
     * The static peers receive plus one more free round as they are being selected "out of order". After the peers are selected, we store back those peers who are left 1 free round.
     * Please note the invariant - every member of this list ends up having 0 or 1 free rounds.
     *
     * Featuring the code owners concept:
     * Every url for review is mapped to some project which has a defined set of code owners, amount of required code owners and an amount of required other (dynamic) peers.
     * This only goes into effect as far as no static peers are selected. Defining static peers means that we explicitly know who should do the review, and overrides these code owner rules completely.
     * The required amount of code owners is pre-selected into the (by definition empty) static peer list. The rest continues as normal. These static peers are given "Free for 1 round"
     * and the dynamic peers are guaranteed not to overlap with the static peer set.
     * The pre-selection of code owners is based on the time since gave a review.
     *
     * @throws ReviewException
     */
    public function create(
        string $group,
        string $me,
        ReviewRequirements $requirements,
        bool $allowRepeat = false,
    ): Review {
        $dynamicPeerCount = $requirements->dynamicPeerCount;
        $projects = $requirements->projects;

        $this->logger->debug('Review start', [
            'me' => $me,
            'group' => $group,
            'dynamicPeerCount' => $dynamicPeerCount,
            'staticPeers' => $requirements->staticPeers,
            'projects' => array_map(fn ($project) => $project->getName(), $projects),
        ]);

        $staticPeerList = $requirements->staticPeers;

        $members = new MemberList($this->peerRepository, $group);
        $lastReview = $this->reviewRepository->top($group);

        if ($lastReview === null) {
            $this->logger->debug('No previous review found, using sentinel');
            $lastReview = (new Review())
                ->setAuthor($me)
                ->setCodeGroup($group)
                ->setPeers([])
                ->setNextPeer((string)$members->get());
        }

        $this->logger->debug('Previous review', [
            'author' => $lastReview->getAuthor(),
            'peers' => $lastReview->getPeers(),
            'nextPeer' => $lastReview->getNextPeer(),
            'skippedPeer' => $lastReview->getSkippedPeer(),
            'freePeers' => $lastReview->getFreePeers(),
        ]);

        $nextReview = (new Review())
            ->setCodeGroup($group)
            ->setAuthor($me)
            ->setSkippedPeer(null);

        // Peers with free round from the previous review get their free round to spend
        $freeForThisRoundPeerList = [];

        foreach ($lastReview->getFreePeers() as $freePeer) {
            $freeForThisRoundPeerList[$freePeer] = 1;
        }

        // Static peers from the group are given one extra free round as they are selected out of order.
        foreach ($staticPeerList as $peerId) {
            if ($members->has($peerId)) {
                $freeForThisRoundPeerList[$peerId] = ($freeForThisRoundPeerList[$peerId] ?? 0) + 1;
            }
        }

        $skippedPeer = $lastReview->getSkippedPeer();
        if ($skippedPeer !== null && !$members->isPeerOnline($skippedPeer)) {
            $this->logger->debug('Skipped peer is offline, good for him');
            $skippedPeer = null;
        }

        $members->rewind($lastReview->getNextPeer());

        $this->logger->debug('Starting dynamic peers lookup');
        $dynamicPeerList = [];
        while (count($dynamicPeerList) < $dynamicPeerCount) {
            if ($skippedPeer === null) { // nobody was skipped
                $peerForReview = (string)$members->getOnline();
                $this->logger->debug('Considering peer ' . $peerForReview);

                if ($peerForReview === $me) { // next one should have been me, I get skipped
                    $peerForReview = (string)$members->getOnline();
                    $skippedPeer = $me;
                    $this->logger->debug('Dude, it\'s me. Considering next peer ' . $peerForReview);
                }
            } else { // someone was skipped
                if ($skippedPeer === $me) { // I was skipped -> I keep being skipped
                    $peerForReview = (string)$members->getOnline();
                    $this->logger->debug('I was skipped last time. Considering peer ' . $peerForReview);

                    if ($peerForReview === $me) {
                        $peerForReview = (string)$members->getOnline();
                        $this->logger->debug('Dude, it\'s me again. Considering next peer ' . $peerForReview);
                    }
                } else { // someone else was skipped -> his turn is now
                    $peerForReview = $skippedPeer;
                    $skippedPeer = null;
                    $this->logger->debug('Considering skipped peer ' . $peerForReview);
                }
            }

            // selected peer is free for this round
            if (($freeForThisRoundPeerList[$peerForReview] ?? 0) > 0) {
                $freeForThisRoundPeerList[$peerForReview] -= 1;
                $this->logger->debug('Selected peer ' . $peerForReview . ' is free for this round');
                continue;
            }

            // If we selected a peer that is a static peer, get another one instead.
            // This static peer will not receive a free round as it was their turn anyways.
            if (in_array($peerForReview, $staticPeerList)) {
                $this->logger->debug('Selected peer ' . $peerForReview . ' is a static peer');
                continue;
            }

            $dynamicPeerList[] = $peerForReview;
        }

        // if the array_unique() removes any duplicate, it indicates that we reused some peers
        if (count(array_unique($dynamicPeerList)) !== $dynamicPeerCount && !$allowRepeat) {
            throw new ReviewException('Not enough online peers');
        }

        // code owner resolving logic
        $codeownerPeerList = [];
        $promotedPeerList = [];

        foreach ($projects as $project) {
            if ($project->getRequiredCodeOwners() > 0) {
                [
                    'codeowners' => $projectCodeOwnerPeerList,
                    'promoted' => $projectPromotedPeerList,
                ] = $this->selectCodeOwners($project, $dynamicPeerList, $me);
                $codeownerPeerList = array_merge($codeownerPeerList, $projectCodeOwnerPeerList);

                foreach ($projectPromotedPeerList as $promotedPeerId) {
                    $pos = array_search($promotedPeerId, $dynamicPeerList, true);
                    unset($dynamicPeerList[$pos]);
                    $promotedPeerList[] = $promotedPeerId;
                }
            }
        }

        // Codeowner peers from the group are given one extra free round as they are selected out of order.
        // But not the promoted dynamic peers, as it would have been their turn anyways
        foreach ($codeownerPeerList as $codeOwnerPeer) {
            $peerId = $codeOwnerPeer->getPeerId();
            if ($members->has($peerId) && !in_array($peerId, $promotedPeerList)) {
                $freeForThisRoundPeerList[$peerId] = ($freeForThisRoundPeerList[$peerId] ?? 0) + 1;
            }
        }

        // Only interested in the peer names with positive count. The count itself is irrelevant
        $freeForNextRoundPeerList = array_keys(
            array_filter($freeForThisRoundPeerList, fn ($count) => $count > 0)
        );

        $staticPeerList = array_map(
            fn ($peerId) => new ReasonForPeer($peerId, 'REQ', ReasonForPeer::REASON_STATIC_SELECT),
            $staticPeerList
        );

        $dynamicPeerList = array_map(
            fn ($peerId) => new ReasonForPeer(
                $peerId,
                (count($requirements->projects) > 1 ? 'ALL' : ''),
                ReasonForPeer::REASON_DYNAMIC_SELECT
            ),
            $dynamicPeerList
        );

        $peerList = array_merge($codeownerPeerList, $staticPeerList, $dynamicPeerList);

        $nextReview
            ->setPeersWithReasons($peerList)
            ->setNextPeer((string) $members->get())
            ->setSkippedPeer($skippedPeer)
            ->setFreePeers($freeForNextRoundPeerList);

        $this->logger->debug('Review selection ended', [
            'author' => $nextReview->getAuthor(),
            'peers' => $nextReview->getPeers(),
            'nextPeer' => $nextReview->getNextPeer(),
            'skippedPeer' => $nextReview->getSkippedPeer(),
            'freePeers' => $nextReview->getFreePeers(),
        ]);

        if (in_array($nextReview->getAuthor(), $nextReview->getPeers())) {
            throw new ReviewException('Assertion failed: I should not review my own code');
        }

        foreach ($dynamicPeerList as $peerWithReason) {
            if (!$members->isPeerOnline($peerWithReason->getPeerId())) {
                throw new ReviewException('Assertion failed: Reviewer is offline');
            }
        }

        return $nextReview;
    }

    /**
     * Logic for deciding the subset of code owners for this review
     * - Any dynamic reviewer that happens to be a CO satisfies the CO requirement
     * - All others are taken from the group of online COs ordered by the time they gave review
     *
     * @throws ReviewException
     * @param string[] $dynamicPeerList
     * @return array{'codeowners': ReasonForPeer[], 'promoted': string[]}
     */
    private function selectCodeOwners(Project $project, array $dynamicPeerList, string $me): array
    {
        $this->logger->debug(
            sprintf(
                'Starting CO lookup for %s with req. of %d',
                $project->getName(),
                $project->getRequiredCodeOwners()
            )
        );

        $codeOwners = [];
        $promotedDynamicPeers = [];
        $requiredOwners = $project->getRequiredCodeOwners();
        /** @var Peer $peer */
        foreach ($project->getPeers() as $peer) {
            $peerId = $peer->getId();

            if ($peerId === $me) {
                continue;
            }

            if (in_array($peerId, $dynamicPeerList, true)) {
                $this->logger->debug(sprintf('Dynamic peer %s is a CO, satisfying one requirement', $peerId));
                $promotedDynamicPeers[] = $peerId;
                $codeOwners[$peerId] = -1;

                continue;
            }

            if (!$peer->isOnline()) {
                continue;
            }

            $latestReview = $this->reviewRepository->getLatestReviewByPeer($peerId);
            $codeOwners[$peerId] = $latestReview?->getCreated()?->getTimestamp() ?? 0;
        }

        asort($codeOwners);

        if (count($codeOwners) < $requiredOwners) {
            throw new ReviewException(sprintf('Unable to satisfy code owners of %s project', $project->getName()));
        }

        return [
            'codeowners' => array_map(
                fn ($peerId) => new ReasonForPeer($peerId, $project->getName(), ReasonForPeer::REASON_PROJECT_CO),
                array_slice(array_keys($codeOwners), 0, $requiredOwners)
            ),
            'promoted' => $promotedDynamicPeers,
        ];
    }
}
