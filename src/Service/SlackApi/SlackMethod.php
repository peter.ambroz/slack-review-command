<?php

declare(strict_types=1);

namespace App\Service\SlackApi;

enum SlackMethod: string
{
    case CONVERSATIONS_SET_TOPIC = 'conversations.setTopic';
    case USERS_INFO = 'users.info';
    case VIEWS_OPEN = 'views.open';
    case VIEWS_PUBLISH = 'views.publish';
    case WORKFLOWS_UPDATE_STEP = 'workflows.updateStep';
    case CHAT_POST_MESSAGE = 'chat.postMessage';
}
