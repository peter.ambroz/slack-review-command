<?php

declare(strict_types=1);

namespace App\Service\SlackApi;

class SlackApiException extends \Exception
{
}