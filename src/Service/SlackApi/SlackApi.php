<?php

declare(strict_types=1);

namespace App\Service\SlackApi;

use Psr\Log\LoggerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class SlackApi
{
    private const HTTP_GET = 'GET';
    private const HTTP_POST = 'POST';

    private const BOT_METHODS = [
        SlackMethod::VIEWS_PUBLISH,
        SlackMethod::WORKFLOWS_UPDATE_STEP,
        SlackMethod::CHAT_POST_MESSAGE,
    ];

    public function __construct(
        private HttpClientInterface $slackApiClient,
        private HttpClientInterface $slackApiBotClient,
        private LoggerInterface $logger,
    ) {}

    /**
     * @throws SlackApiException
     */
    public function get(SlackMethod $method, array $query): array
    {
        return $this->call(self::HTTP_GET, $method, [
            'query' => $query,
        ]);
    }

    /**
     * @throws SlackApiException
     */
    public function post(SlackMethod $method, array $payload): array
    {
        return $this->call(self::HTTP_POST, $method, [
            'json' => $payload,
        ]);
    }

    /**
     * @throws SlackApiException
     */
    private function call(string $httpMethod, SlackMethod $method, array $options): array
    {
        $this->logger->info('Call ' . $httpMethod . ' ' . $method->value, $options);
        try {
            if (in_array($method, self::BOT_METHODS, true)) {
                $response = $this->slackApiBotClient->request($httpMethod, $method->value, $options);
            } else {
                $response = $this->slackApiClient->request($httpMethod, $method->value, $options);
            }
            $responseData = $response->toArray();
        } catch (\Exception $e) {
            throw new SlackApiException('Call to API failed: ' . $e->getMessage());
        }

        if (!$responseData['ok']) {
            throw new SlackApiException($responseData['error']);
        }

        return $responseData;
    }
}
