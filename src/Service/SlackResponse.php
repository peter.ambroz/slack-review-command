<?php

declare(strict_types=1);

namespace App\Service;

use Symfony\Component\HttpFoundation\JsonResponse;

class SlackResponse extends JsonResponse
{
    private ?string $asyncUrl = null;

    private ?string $triggerId = null;

    public function __construct(
        private string $text,
        private ResponseType $type = ResponseType::PRIVATE,
        private ?array $payload = null,
        private readonly ?array $blocks = null,
    ) {
        parent::__construct($this->getData());
    }

    public function isAsync(): bool
    {
        return $this->type === ResponseType::CHANNEL || $this->type === ResponseType::MODAL;
    }

    public function isModal(): bool
    {
        return $this->type === ResponseType::MODAL;
    }

    public function getData(): array
    {
        $data = [
            'response_type' => $this->type->value,
            'text' => $this->text,
        ];
        if ($this->blocks) {
            $data = array_merge($data, ['blocks' => $this->blocks]);
        }
        return $data;
    }

    public function getPayload(): ?array
    {
        return $this->payload;
    }

    public function getAsyncUrl(): ?string
    {
        return $this->asyncUrl;
    }

    public function setAsyncUrl(string $asyncUrl): self
    {
        $this->asyncUrl = $asyncUrl;
        return $this;
    }

    public function getTriggerId(): ?string
    {
        return $this->triggerId;
    }

    public function setTriggerId(?string $triggerId): self
    {
        $this->triggerId = $triggerId;
        return $this;
    }
}
