<?php

declare(strict_types=1);

namespace App\Service;

use Symfony\Component\Validator\Constraints as Assert;

class SlackCommandData
{
    /** @Assert\NotBlank */
    private string $channelName;

    private ?string $channelId = null;

    /** @Assert\NotBlank */
    private string $command;

    private string $text = '';

    private ?string $responseUrl = null;

    /** @Assert\NotBlank */
    private string $userId;

    private ?string $userName = null;

    private ?string $triggerId = null;

    public function getChannelName(): ?string
    {
        return $this->channelName;
    }

    public function setChannelName(string $channelName): self
    {
        $this->channelName = $channelName;
        return $this;
    }

    public function getChannelId(): ?string
    {
        return $this->channelId;
    }

    public function setChannelId(?string $channelId): self
    {
        $this->channelId = $channelId;
        return $this;
    }

    public function getCommand(): ?string
    {
        return $this->command;
    }

    public function setCommand(string $command): self
    {
        $this->command = $command;
        return $this;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = trim($text);
        return $this;
    }

    public function getResponseUrl(): ?string
    {
        return $this->responseUrl;
    }

    public function setResponseUrl(?string $responseUrl): self
    {
        $this->responseUrl = $responseUrl;
        return $this;
    }

    public function getUserId(): ?string
    {
        return $this->userId;
    }

    public function setUserId(string $userId): self
    {
        $this->userId = $userId;
        return $this;
    }

    public function getUserName(): ?string
    {
        return $this->userName;
    }

    public function setUserName(?string $userName): self
    {
        $this->userName = $userName;
        return $this;
    }

    public function getTriggerId(): ?string
    {
        return $this->triggerId;
    }

    public function setTriggerId(?string $triggerId): self
    {
        $this->triggerId = $triggerId;
        return $this;
    }
}
