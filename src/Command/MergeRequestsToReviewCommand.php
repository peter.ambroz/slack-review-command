<?php

declare(strict_types=1);

namespace App\Command;

use App\Entity\Peer;
use App\Repository\PeerRepository;
use App\Service\Gitlab\MergeRequestService;
use App\Service\SlackApi\SlackApi;
use App\Service\SlackApi\SlackMethod;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(name: 'app:reviews', description: 'Get merge request to review and send them to slack channel')]
class MergeRequestsToReviewCommand extends Command
{
    public function __construct(
        private readonly PeerRepository $peerRepository,
        private readonly MergeRequestService $mergeRequestService,
        private readonly SlackApi $slackApi,
    ) {
        parent::__construct();
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $peers = $this->getPeers($input->getArgument('slackPeerId'));

        foreach ($peers as $peer) {
            $reviews = $this->mergeRequestService->getMergeRequestsToApprove($peer);
            if (count($reviews) === 0) {
                continue;
            }
            $request = [
                'channel' => $peer->getId(),
                'blocks' => $reviews
            ];

            $this->slackApi->post(SlackMethod::CHAT_POST_MESSAGE, $request);
        }
        return Command::SUCCESS;
    }

    protected function configure()
    {
        $this->addArgument('slackPeerId', InputArgument::OPTIONAL, 'Run command with only selected user');
    }

    /**
     * @return Peer[]
     */
    private function getPeers(?string $slackId): array
    {
        if ($slackId) {
            return $this->peerRepository->findBy(['id' => $slackId]);
        }
        return $this->peerRepository->getOnlinePeersWithEnabledReminder();
    }
}
