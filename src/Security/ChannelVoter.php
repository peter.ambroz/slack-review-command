<?php

declare(strict_types=1);

namespace App\Security;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class ChannelVoter extends Voter
{
    protected function supports(string $attribute, $subject): bool
    {
        return $attribute === 'CHANNEL';
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $allowedChannels = explode(',', $_ENV['SLACK_CHANNELS']);
        if (count($allowedChannels) && ($allowedChannels[0] !== '*')) {
            return in_array($subject, $allowedChannels, true);
        }

        return true;
    }
}
