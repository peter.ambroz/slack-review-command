<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\MemberRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: MemberRepository::class)]
#[ORM\Index(columns: ['code_group'], name: 'idx_codegroup')]
#[ORM\Index(columns: ['sort_order'], name: 'idx_sortorder')]
class Member
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;

    #[ORM\ManyToOne(targetEntity: 'Peer', inversedBy: 'members')]
    #[ORM\JoinColumn(nullable: false)]
    private Peer $peer;

    #[ORM\Column(type: 'string', length: 255)]
    private string $codeGroup;

    #[ORM\Column(type: 'integer')]
    private int $sortOrder;

    public function __construct()
    {
        $this->sortOrder = 0;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPeer(): ?Peer
    {
        return $this->peer;
    }

    public function setPeer(Peer $peer): self
    {
        $this->peer = $peer;

        return $this;
    }

    public function getCodeGroup(): ?string
    {
        return $this->codeGroup;
    }

    public function setCodeGroup(string $codeGroup): self
    {
        $this->codeGroup = $codeGroup;

        return $this;
    }

    public function getSortOrder(): ?int
    {
        return $this->sortOrder;
    }

    public function setSortOrder(int $sortOrder): self
    {
        $this->sortOrder = $sortOrder;

        return $this;
    }

    public function __toString(): string
    {
        return $this->peer->getId();
    }
}
