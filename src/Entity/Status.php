<?php

declare(strict_types=1);

namespace App\Entity;

enum Status: string
{
    public const OFFLINE_STATUSES = [self::VACATION, self::SICK];

    case VACATION = 'vacation';
    case SICK = 'sick';
}
