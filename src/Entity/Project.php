<?php

namespace App\Entity;

use App\Repository\ProjectRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ProjectRepository::class)]
#[ORM\Index(columns: ['uri'], name: 'idx_uri')]
#[ORM\Index(columns: ['priority'], name: 'idx_priority')]
#[ORM\Index(columns: ['name'], name: 'idx_name')]
#[ORM\Index(columns: ['code_group'], name: 'idx_codegroup')]
class Project
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;

    #[ORM\Column(type: 'string', length: 255)]
    private string $uri;

    #[ORM\Column(type: 'integer')]
    private int $requiredCodeOwners;

    #[ORM\Column(type: 'integer')]
    private int $requiredOthers;

    #[ORM\ManyToMany(targetEntity: 'Peer', inversedBy: 'projects')]
    private Collection $peers;

    /**
     * Higher number = higher priority
     */
    #[ORM\Column(type: 'integer')]
    private int $priority;

    #[ORM\Column(type: 'string', length: 255)]
    private string $name;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $codeGroup = null;

    public function __construct()
    {
        $this->peers = new ArrayCollection();
        $this->priority = 0;
        $this->name = 'default';
        $this->requiredCodeOwners = 0;
        $this->requiredOthers = 1;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUri(): ?string
    {
        return $this->uri;
    }

    public function setUri(string $uri): self
    {
        $this->uri = $uri;
        return $this;
    }

    public function getRequiredCodeOwners(): ?int
    {
        return $this->requiredCodeOwners;
    }

    public function setRequiredCodeOwners(int $requiredCodeOwners): self
    {
        $this->requiredCodeOwners = $requiredCodeOwners;
        return $this;
    }

    public function getRequiredOthers(): ?int
    {
        return $this->requiredOthers;
    }

    public function setRequiredOthers(int $requiredOthers): self
    {
        $this->requiredOthers = $requiredOthers;
        return $this;
    }

    public function getPeers(): Collection
    {
        return $this->peers;
    }

    public function addPeer(Peer $peer): self
    {
        if (!$this->peers->contains($peer)) {
            $this->peers[] = $peer;
        }

        return $this;
    }

    public function removePeer(Peer $peer): self
    {
        $this->peers->removeElement($peer);

        return $this;
    }

    public function getPriority(): ?int
    {
        return $this->priority;
    }

    public function setPriority(int $priority): self
    {
        $this->priority = $priority;
        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function getCodeGroup(): ?string
    {
        return $this->codeGroup;
    }

    public function setCodeGroup(?string $codeGroup): self
    {
        $this->codeGroup = $codeGroup;

        return $this;
    }
}
