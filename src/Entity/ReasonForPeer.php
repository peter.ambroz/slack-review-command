<?php

declare(strict_types=1);

namespace App\Entity;

class ReasonForPeer
{
    public const REASON_PROJECT_CO = 'CO';
    public const REASON_STATIC_SELECT = 'S';
    public const REASON_DYNAMIC_SELECT = 'D';

    public function __construct(
        private string $peerId,
        private string $reasonText,
        private string $reasonType,
    ) {}

    public function getPeerId(): string
    {
        return $this->peerId;
    }

    public function getReasonText(): string
    {
        return $this->reasonText;
    }

    public function getReasonType(): string
    {
        return $this->reasonType;
    }
}
