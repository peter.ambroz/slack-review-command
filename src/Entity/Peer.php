<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\PeerRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: PeerRepository::class)]
#[ORM\Index(columns: ['online'], name: 'idx_online')]
#[ORM\Index(columns: ['gitlab_id'], name: 'idx_gitlab_id')]
class Peer
{
    public const REVIEW_REMINDER = 'review_reminder';

    #[ORM\Id]
    #[ORM\Column(type: 'string', length: 32)]
    private ?string $id = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $fullName = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $defaultGroup = null;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    private ?\DateTimeImmutable $homeUpdated = null;

    #[ORM\Column(type: 'json')]
    private array $settings = [];

    #[ORM\OneToMany(mappedBy: 'peer', targetEntity: 'Member')]
    private Collection $members;

    #[ORM\ManyToMany(targetEntity: 'Project', mappedBy: 'peers')]
    private Collection $projects;

    #[ORM\Column(type: 'boolean')]
    private bool $online;

    #[ORM\Column(type: 'integer', nullable: true)]
    private ?int $gitlabId = null;

    #[ORM\Column(type: 'string', nullable: true, enumType: Status::class)]
    private ?Status $status = null;

    public function __construct()
    {
        $this->members = new ArrayCollection();
        $this->projects = new ArrayCollection();
        $this->online = true;
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function setId(string $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getDefaultGroup(): ?string
    {
        return $this->defaultGroup;
    }

    public function setDefaultGroup(?string $defaultGroup): self
    {
        $this->defaultGroup = $defaultGroup;
        return $this;
    }

    public function getFullName(): ?string
    {
        return $this->fullName;
    }

    public function setFullName(string $fullName): self
    {
        $this->fullName = $fullName;
        return $this;
    }

    public function getSettings(): ?array
    {
        return $this->settings;
    }

    public function setSettings(array $settings): self
    {
        $this->settings = $settings;
        return $this;
    }

    public function addSetting(string $key, $value): void
    {
        $this->settings[$key] = $value;
    }

    public function getSetting(string $key, $default = null)
    {
        return $this->settings[$key] ?? $default;
    }

    public function getHomeUpdated(): ?\DateTimeImmutable
    {
        return $this->homeUpdated;
    }

    public function setHomeUpdated(\DateTimeImmutable $homeUpdated): self
    {
        $this->homeUpdated = $homeUpdated;
        return $this;
    }

    public function getMembers(): Collection
    {
        return $this->members;
    }

    public function isOnline(): bool
    {
        return $this->online && !in_array($this->status, Status::OFFLINE_STATUSES);
    }

    public function setOnline(bool $online): self
    {
        $this->online = $online;

        return $this;
    }

    public function __toString(): string
    {
        return $this->id;
    }

    public function getProjects(): Collection
    {
        return $this->projects;
    }

    public function setGitlabId(?int $gitlabId): void
    {
        $this->gitlabId = $gitlabId;
    }

    public function getGitlabId(): ?int
    {
        return $this->gitlabId;
    }

    public function addProject(Project $project): self
    {
        if (!$this->projects->contains($project)) {
            $this->projects[] = $project;
            $project->addPeer($this);
        }

        return $this;
    }

    public function removeProject(Project $project): self
    {
        if ($this->projects->removeElement($project)) {
            $project->removePeer($this);
        }

        return $this;
    }

    public function getStatus(): ?Status
    {
        return $this->status;
    }

    public function setStatus(?Status $status): void
    {
        $this->status = $status;
    }
}
