<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\ReviewRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ReviewRepository::class)]
#[ORM\Index(columns: ['code_group'], name: 'idx_codegroup')]
#[ORM\Index(columns: ['peers'], name: 'idx_peers')]
#[ORM\Index(columns: ['author'], name: 'idx_author')]
#[ORM\Index(columns: ['created'], name: 'idx_created')]
class Review
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;

    /**
     * @var string[]
     */
    #[ORM\Column(type: 'json')]
    private array $peers = [];

    /**
     * @var string[]
     */
    #[ORM\Column(type: 'json')]
    private array $reasons = [];

    #[ORM\Column(type: 'string', length: 255)]
    private string $author;

    #[ORM\Column(type: 'string', length: 255)]
    private string $codeGroup;

    #[ORM\Column(type: 'text')]
    private string $content;

    #[ORM\Column(type: 'datetime_immutable')]
    private \DateTimeImmutable $created;

    #[ORM\Column(type: 'string', length: 255)]
    private string $nextPeer;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $skippedPeer = null;

    /**
     * @var string[]
     */
    #[ORM\Column(type: 'json')]
    private array $freePeers = [];

    /**
     * @var ReasonForPeer[]|null
     */
    private ?array $peersWithReason = null;

    public function __construct()
    {
        $this->created = new \DateTimeImmutable();
        $this->content = '';
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string[]|null
     */
    public function getPeers(): ?array
    {
        return $this->peers;
    }

    /**
     * @param string[] $peers
     */
    public function setPeers(array $peers): self
    {
        $this->peers = $peers;

        return $this;
    }

    /**
     * @return string[]|null
     */
    public function getReasons(): ?array
    {
        return $this->reasons;
    }

    /**
     * @param string[] $reasons
     */
    public function setReasons(array $reasons): self
    {
        $this->reasons = $reasons;

        return $this;
    }

    public function getCodeGroup(): ?string
    {
        return $this->codeGroup;
    }

    public function setCodeGroup(string $codeGroup): self
    {
        $this->codeGroup = $codeGroup;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getCreated(): ?\DateTimeImmutable
    {
        return $this->created;
    }

    public function setCreated(\DateTimeImmutable $created): self
    {
        $this->created = $created;

        return $this;
    }

    public function getAuthor(): string
    {
        return $this->author;
    }

    public function setAuthor(string $author): self
    {
        $this->author = $author;
        return $this;
    }

    public function getSkippedPeer(): ?string
    {
        return $this->skippedPeer;
    }

    public function setSkippedPeer(?string $skippedPeer): self
    {
        $this->skippedPeer = $skippedPeer;
        return $this;
    }

    public function getNextPeer(): ?string
    {
        return $this->nextPeer;
    }

    public function setNextPeer(string $nextPeer): self
    {
        $this->nextPeer = $nextPeer;
        return $this;
    }

    /**
     * @return string[]|null
     */
    public function getFreePeers(): ?array
    {
        return $this->freePeers;
    }

    /**
     * @param string[] $freePeers
     */
    public function setFreePeers(array $freePeers): self
    {
        $this->freePeers = $freePeers;

        return $this;
    }

    /**
     * @return ReasonForPeer[]
     */
    public function getPeersWithReason(): array
    {
        if ($this->peersWithReason !== null) {
            return $this->peersWithReason;
        }

        return $this->peersWithReason = array_map(
            fn (string $peerId, array $reason) => new ReasonForPeer($peerId, $reason['text'], $reason['type']),
            $this->peers,
            $this->reasons
        );
    }

    /**
     * @param ReasonForPeer[] $peers
     */
    public function setPeersWithReasons(array $peers): self
    {
        $this->peersWithReason = $peers;

        $this->setPeers(array_map(fn ($reasonedPeer) => $reasonedPeer->getPeerId(), $peers));
        $this->setReasons(
            array_map(
                fn ($reasonedPeer) => ['type' => $reasonedPeer->getReasonType(), 'text' => $reasonedPeer->getReasonText()],
                $peers
            )
        );

        return $this;
    }
}
