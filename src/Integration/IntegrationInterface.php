<?php

namespace App\Integration;

use App\Service\Command\Command;

interface IntegrationInterface
{
    public function supports(Command $command): bool;

    //cannot declare type since data from different commands cannot be normalized to single DTO
    public function run($data): void;

    public function isPropagationStopped(): bool;
}