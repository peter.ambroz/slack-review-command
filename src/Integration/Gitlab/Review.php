<?php

declare(strict_types=1);

namespace App\Integration\Gitlab;

class Review
{
    /** @var int[] $peerIds */
    public function __construct(
        private readonly string $url,
        private readonly array $peerIds
    )
    {
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @return int[]
     */
    public function getPeerIds(): array
    {
        return $this->peerIds;
    }
}
