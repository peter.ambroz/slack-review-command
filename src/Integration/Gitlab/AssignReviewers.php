<?php

declare(strict_types=1);

namespace App\Integration\Gitlab;

use App\Entity\Peer;
use App\Entity\Project;
use App\Entity\ReasonForPeer;
use App\Entity\Review;
use App\Integration\Gitlab\Review as ReviewDto;
use App\Integration\IntegrationInterface;
use App\Repository\PeerRepository;
use App\Repository\ProjectRepository;
use App\Service\Command\Command;
use App\Service\GitlabApi\GitlabApi;
use Psr\Log\LoggerInterface;

final class AssignReviewers implements IntegrationInterface
{
    public function __construct(
        private readonly GitlabApi $gitlabApi,
        private readonly PeerRepository $peerRepository,
        private readonly LoggerInterface $logger,
        private readonly ProjectRepository $projectRepository,
    ) {
    }

    public function supports(Command $command): bool
    {
        return $command::getName() === 'setNext';
    }

    /**
     * @param Review $data
     */
    public function run($data): void
    {
        $reviews = $this->parseReviews($data->getContent(), $data->getPeers(), $data->getReasons());

        foreach ($reviews as $review) {
            $idsCount = count(array_filter($review->getPeerIds()));
            if ($idsCount !== count($review->getPeerIds())) {
                $this->logger->warning(sprintf('Well this is awkward some of peers doesn\'t have gitlab ids for url %s', $review->getUrl()));
            }
            if ($idsCount === 0) {
                $this->logger->warning('Even worse, none of selected peers have gitlab ids - skipping assignment');
                continue;
            }
            $this->gitlabApi->assignReviewers($review->getUrl(), $review->getPeerIds());
        }
    }

    public function isPropagationStopped(): bool
    {
        return false;
    }

    /**
     * @return ReviewDto[]
     */
    private function parseReviews(?string $data, array $peers, array $reasons): array
    {
        if (!$data) {
            return [];
        }

        preg_match_all('/<([^>]+)>/', $data, $reviewUrls);
        array_shift($reviewUrls);
        $reviewUrls = [...$reviewUrls[0]];

        if (count($reviewUrls) === 1) {
            return [new ReviewDto($reviewUrls[0], $this->convertPeerIdsToGitlabsIds($peers))];
        }

        $peers = $this->assignIdsToPeers($peers, $reasons);
        $selectedPeers = $codeOwners = [];

        foreach ($peers as $peer) {
            switch ($peer['type']) {
                case ReasonForPeer::REASON_STATIC_SELECT:
                case ReasonForPeer::REASON_DYNAMIC_SELECT:
                    $selectedPeers[] = $peer;
                    break;
                case ReasonForPeer::REASON_PROJECT_CO:
                    $codeOwners[] = $peer;
                    break;
            }
        }

        $reviews = [];

        foreach ($reviewUrls as $url) {
            if (str_contains($url, 'git.websupport.sk')) {
                $this->logger->warning('We don\'t support legacy URLs yet');
                continue;
            }
            $project = $this->projectRepository->matchUri($this->getProjectUri($url));
            $codeOwner = $this->getCodeOwnerForProject($codeOwners, $project);

            $reviewers = array_merge([$codeOwner], $selectedPeers);
            $reviewers = array_filter($reviewers);
            $reviewers = array_column($reviewers, 'id');

            $reviews[] = new ReviewDto($url, $this->convertPeerIdsToGitlabsIds($reviewers));
        }

        return $reviews;
    }

    private function convertPeerIdsToGitlabsIds(array $peerIds): array
    {
        return array_map(
            fn(Peer $peer) => $peer->getGitlabId(),
            $this->peerRepository->getGitlabIds($peerIds)
        );
    }

    private function assignIdsToPeers(array $peers, array $reasons): array
    {
        foreach ($reasons as $index => &$reason) {
            $reason['id'] = $peers[$index];
        }
        return $reasons;
    }

    private function getProjectUri(string $url): string
    {
        preg_match('#https://[^>/]+(.*)#', $url, $matches);
        return $matches[1];
    }

    private function getCodeOwnerForProject(array $codeOwners, Project $project): array
    {
        foreach ($codeOwners as $codeOwner) {
            if ($codeOwner['text'] === $project->getName()) {
                return $codeOwner;
            }
        }
        return [];
    }
}
