<?php

declare(strict_types=1);

namespace App\EventSubscriber;

use App\Event\CommandExecutedEventInterface;
use App\Event\SetNextCommandExecutedEvent;
use App\Integration\IntegrationInterface;
use Symfony\Component\DependencyInjection\ServiceLocator;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;

class IntegrationSubscriber implements EventSubscriberInterface
{
    /** @var CommandExecutedEventInterface[] */
    private static array $executedCommands = [];

    public function __construct(private readonly ServiceLocator $integrations)
    {
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::TERMINATE => 'processIntegrations',
            SetNextCommandExecutedEvent::class => 'storeExecutedCommand'
        ];
    }

    public function storeExecutedCommand(CommandExecutedEventInterface $commandExecutedEvent)
    {
        self::$executedCommands[] = $commandExecutedEvent;
    }

    public function processIntegrations()
    {
        if (empty(self::$executedCommands)) {
            return;
        }
        foreach (self::$executedCommands as $command) {
            foreach ($this->integrations->getProvidedServices() as $integration) {
                $integration = $this->integrations->get($integration);
                /** @var IntegrationInterface $integration */
                if (!$integration->supports($command->getCommand())) {
                    continue;
                }
                $integration->run($command->getData());
                if ($integration->isPropagationStopped()) {
                    return;
                }
            }
        }
    }

    public function __destruct()
    {
        //Is this even necessary?
        self::$executedCommands = [];
    }
}
