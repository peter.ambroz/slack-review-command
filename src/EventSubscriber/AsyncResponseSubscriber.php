<?php

declare(strict_types=1);

namespace App\EventSubscriber;

use App\Service\ResponseQueue;
use App\Service\SlackApi\SlackApi;
use App\Service\SlackApi\SlackMethod;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\TerminateEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class AsyncResponseSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private ResponseQueue $queue,
        private HttpClientInterface $httpClient,
        private SlackApi $slackApi,
        private LoggerInterface $logger
    ) {}

    public function sendAsyncResponses(TerminateEvent $event)
    {
        if ($this->queue->count() > 0) {
            $this->logger->log(LogLevel::INFO, 'Sending {amount} async responses', ['amount' => $this->queue->count()]);

            while (($response = $this->queue->dequeue()) !== null) {
                if ($response->isModal()) {
                    $this->slackApi->post(SlackMethod::VIEWS_OPEN, [
                        'trigger_id' => $response->getTriggerId(),
                        'view' => $response->getPayload(),
                    ]);
                } else {
                    $this->httpClient->request('POST', $response->getAsyncUrl(), [
                        'json' => $response->getData(),
                    ]);
                }
            }
        }
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::TERMINATE => 'sendAsyncResponses',
        ];
    }
}
