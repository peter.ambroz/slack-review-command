<?php

declare(strict_types=1);

namespace App\Form;

use App\Service\SlackCommandData;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SlackCommandType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('channel_id')
            ->add('channel_name')
            ->add('user_id')
            ->add('user_name')
            ->add('command')
            ->add('text', TextType::class, ['empty_data' => ''])
            ->add('trigger_id')
            ->add('response_url');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'allow_extra_fields' => true,
            'data_class' => SlackCommandData::class,
            'csrf_protection' => false,
        ]);
    }
}
