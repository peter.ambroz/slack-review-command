<?php

declare(strict_types=1);

namespace App\Event;

use App\Entity\Review;
use App\Service\Command\Command;
use Symfony\Contracts\EventDispatcher\Event;

class SetNextCommandExecutedEvent extends Event implements CommandExecutedEventInterface
{
    public function __construct(private readonly Command $command, private readonly Review $commandData)
    {
    }

    public function getCommand(): Command
    {
        return $this->command;
    }

    public function getData(): Review
    {
        return $this->commandData;
    }
}
