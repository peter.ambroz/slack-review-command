<?php

namespace App\Event;

use App\Service\Command\Command;

interface CommandExecutedEventInterface
{
    public function getCommand(): Command;

    public function getData(): mixed;
}