<?php

namespace App\Repository;

use App\Entity\Project;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NoResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Project|null find($id, $lockMode = null, $lockVersion = null)
 * @method Project|null findOneBy(array $criteria, array $orderBy = null)
 * @method Project[]    findAll()
 * @method Project[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProjectRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Project::class);
    }

    public function matchUri(string $uri, ?string $codeGroup = null): Project
    {
        try {
            $qb = $this->createQueryBuilder('p')
                ->where(':uri LIKE p.uri')
                ->orderBy('p.priority', 'DESC')
                ->setParameter('uri', $uri);
            if ($codeGroup) {
                $qb->andWhere('p.codeGroup = :codegroup')
                    ->setParameter('codegroup', $codeGroup);
            }
            return $qb->setMaxResults(1)
                ->getQuery()->getSingleResult();
        } catch (NoResultException) {
            return new Project();
        }
    }
}
