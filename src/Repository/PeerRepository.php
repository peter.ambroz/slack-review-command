<?php

namespace App\Repository;

use App\Entity\Member;
use App\Entity\Peer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Peer|null find($id, $lockMode = null, $lockVersion = null)
 * @method Peer|null findOneBy(array $criteria, array $orderBy = null)
 * @method Peer[]    findAll()
 * @method Peer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PeerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Peer::class);
    }

    public function findOrCreate(string $id, ?string $defaultGroup = null, bool $online = true): Peer
    {
        $peer = $this->find($id);
        if ($peer === null) {
            $peer = (new Peer())
                ->setId($id)
                ->setDefaultGroup($defaultGroup)
                ->setOnline($online);

            $this->getEntityManager()->persist($peer);
            $this->getEntityManager()->flush();
        }

        return $peer;
    }

    /**
     * @return Peer[]
     */
    public function parsePeerList(string $peerList, ?string $defaultGroup = null): array
    {
        preg_match_all('/(?<offline_char>\(?)<@(?<peer_id>[^|>]+)[>|]\)?/', $peerList, $matchedPeers);

        return array_map(function (string $peerId, string $offlineChar) use ($defaultGroup): Peer {
            return $this->findOrCreate($peerId, $defaultGroup, strlen($offlineChar) === 0);
        }, $matchedPeers['peer_id'], $matchedPeers['offline_char']);
    }

    public function toggle(string $peerId, bool $online): void
    {
        $qb = $this->createQueryBuilder('p')
            ->update()
            ->set('p.online', ':online')
            ->where('p.id = :peer')
            ->setParameter('online', $online)
            ->setParameter('peer', $peerId);

        $qb->getQuery()->execute();
    }

    /**
     * @return Peer[]
     */
    public function listPeers(string $group, ?bool $online = null): array
    {
        $qb = $this->createQueryBuilder('p')
            ->select('p')
            ->join('p.members', 'm')
            ->where('m.codeGroup = :group')
            ->setParameter('group', $group)
            ->orderBy('m.sortOrder', 'ASC');

        if ($online !== null) {
            $qb
                ->andWhere('p.online = :online')
                ->setParameter('online', $online);
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @return Peer[]
     */
    public function getOnlinePeersWithEnabledReminder(): array
    {
        $peers = $this->createQueryBuilder('p')
            ->select('p')
            ->andWhere('p.gitlabId IS NOT NULL')
            ->andWhere("JSON_EXTRACT(p.settings, '$.review_reminder') = 1")
            ->getQuery()
            ->getResult();

        return array_filter($peers, fn(Peer $peer) => $peer->isOnline());
    }

    /**
     * @param string[] $peerIds
     * @return Peer[]
     */
    public function getGitlabIds(array $peerIds): array
    {
        return $this->createQueryBuilder('p')
            ->select('p')
            ->where('p.id IN (:peerIds)')
            ->setParameter('peerIds', $peerIds)
            ->getQuery()
            ->getResult();
    }

    public function flush(): void
    {
        $this->getEntityManager()->flush();
    }
}
