<?php

namespace App\Repository;

use App\Entity\Review;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Review|null find($id, $lockMode = null, $lockVersion = null)
 * @method Review|null findOneBy(array $criteria, array $orderBy = null)
 * @method Review[]    findAll()
 * @method Review[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReviewRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Review::class);
    }

    public function top(?string $group = null): ?Review
    {
        return $this->findOneBy(
            ($group === null) ? [] : ['codeGroup' => $group],
            ['id' => 'DESC']
        );
    }

    /**
     * @return Review[]
     */
    public function getLatestReviewsByAuthor(string $peer, int $limit = 1): array
    {
        return $this->findBy(
            ['author' => $peer],
            ['id' => 'DESC'],
            $limit
        );
    }

    public function getLatestReviewByPeer(string $peer): ?Review
    {
        try {
            return $this->createQueryBuilder('r')
                ->where('JSON_CONTAINS(r.peers, :peer) = 1')
                ->orderBy('r.id', 'DESC')
                ->setParameter('peer', sprintf('"%s"', $peer))
                ->setMaxResults(1)
                ->getQuery()->getSingleResult();
        } catch (NoResultException | NonUniqueResultException) {
            return null;
        }
    }

    public function add(Review $review): void
    {
        $this->getEntityManager()->persist($review);
        $this->getEntityManager()->flush();
    }

    public function remove(Review $review): void
    {
        $this->getEntityManager()->remove($review);
        $this->getEntityManager()->flush();
    }
}
