<?php

namespace App\Repository;

use App\Entity\Member;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Member|null find($id, $lockMode = null, $lockVersion = null)
 * @method Member|null findOneBy(array $criteria, array $orderBy = null)
 * @method Member[]    findAll()
 * @method Member[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MemberRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Member::class);
    }

    /**
     * @return string[]
     */
    public function listGroups(string $member = null): array
    {
        $qb = $this->createQueryBuilder('m')
            ->select('m.codeGroup')
            ->distinct();

        if ($member !== null) {
            $qb
                ->where('m.peer = :member')
                ->setParameter('member', $member);
        }

        return array_map(static function (array $x): string {
            return $x['codeGroup'];
        }, $qb->getQuery()->getArrayResult());
    }

    public function dropMembers(string $group): void
    {
        $this->createQueryBuilder('m')
            ->delete()
            ->where('m.codeGroup = :group')
            ->setParameter('group', $group)
            ->getQuery()->execute();
    }

    public function add(Member $member): void
    {
        $this->getEntityManager()->persist($member);
    }

    public function flush(): void
    {
        $this->getEntityManager()->flush();
    }
}
